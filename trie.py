class Node():
    def __init__(self, char=None, value=None, parent=None):
        self.children = {}
        self.char = char
        self.parent = parent
        self.is_final = False

    def __str__(self):
        return "{}".format(self.char)

    def __repr__(self):
        return "{}".format(self.char)

       
class Trie:

    def __init__(self):
        self.num_nodes = 0
        self.root_node = Node()

    def insert(self, key):
        # Start at the root node
        node = self.root_node

        for char in key:
            if char not in node.children:
                node.children[char] = Node(char=char, parent=node)

            node = node.children[char]
        
        node.is_final = True
        node.word = key

    def find(self, key):
        # Start at the root node
        node = self.root_node

        for char in key:
            if char in node.children:
                node = node.children[char]
            else:
                return None

        return node
