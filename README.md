# Task To Do

- [x] La tarea en este apartado consistira en: Generar un diccionario en trie
  con el vocabulario del fichero ”quijote.txt”. Cargar el fichero, limpiar el
  texto, extraer el vocabulario y generar un trie. Se debera guardar en
  memoria secundaria para facilitar su uso posterior tanto el trie como una
  lista

- [x] Los algoritmos para el calculo de la distancia de Levenshtein y la
  distancia de Damerau-Levenshtein entre dos cadenas. Se debera hacer un bucle
  sobre todo el vocabulario para encontrar los terminos cuya distancia
  respecto de la cadena dada sea menor que un umbral determinado.  El algoritmo
  para el calculo de la distancia de Levenshtein entre una cadena 

- [x] El algoritmo para el calculo de la distancia de Levenshtein entre una
  cadena y un conjunto de cadenas representado como un trie mediante la
  ramificacion y poda de estados.

- [x] Hacer un estudio del coste temporal de todas las soluciones desarrolladas
  y elegir la mas eficiente.
