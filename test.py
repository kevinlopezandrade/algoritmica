import os
import ALT_library
import trie

from distances import *

def parse_line(line):
    """Takes a line and return an array with all the elements"""
    line = line.replace('\t', ' ') 
    line = line.replace('\n', ' ')

    return line.split(' ')


def check_distances(file_path, vocabulary, distance_function):

    with open(file_path) as file:
        for line in file:
            contents = parse_line(line)

            word = contents[0]
            threshold = int(contents[1])
            total = contents[2]
            terms = contents[3:]

            retrieved_terms = ALT_library.search_below_threshold(
                word, 
                vocabulary, 
                threshold, 
                distance_function,
            )

            print(word, threshold, total, len(retrieved_terms))


            for term, our_term in zip(terms, retrieved_terms):
                if term != our_term:
                    raise ValueError("Error")

def check_distances_trie(file_path, trie, distance_function):
    """This method is extremly inneficient but before
    we implement the branch and bound we cannot do better"""

    with open(file_path) as file:
        for line in file:
            contents = parse_line(line)

            word = contents[0]
            threshold = int(contents[1])
            total = contents[2]
            terms = contents[3:]

            distance_dict = distance_function(word, trie)

            retrieved_terms = []

            for key, value in distance_dict.items():
                if value <= threshold:
                    retrieved_terms.append("{}:{}".format(value, key))

            retrieved_terms.sort()

            print(word, threshold, total, len(retrieved_terms))

            for term, our_term in zip(terms, retrieved_terms):
                if term != our_term:
                    print(retrieved_terms)
                    print(terms)
                    raise ValueError("Error in {}".format(word))


def check_distances_trie_bb(file_path, trie, distance_function):
    """This method is extremly inneficient but before
    we implement the branch and bound we cannot do better"""

    with open(file_path) as file:
        for line in file:
            contents = parse_line(line)

            word = contents[0]
            threshold = int(contents[1])
            total = contents[2]
            terms = contents[3:]

            distance_dict = distance_function(word, trie, threshold)

            retrieved_terms = []

            for key, value in distance_dict.items():
                retrieved_terms.append("{}:{}".format(value, key))

            retrieved_terms.sort()

            print(word, threshold, total, len(retrieved_terms))

            for term, our_term in zip(terms, retrieved_terms):
                if term != our_term:
                    breakpoint()
                    print(retrieved_terms)
                    print(terms)
                    raise ValueError("Error in {}".format(word))


if __name__ == "__main__":
    PATH = "./quijote.txt"

    vocabulary = ALT_library.get_vocabulary(PATH)

    # vocabulary = ["pataat"]

    trie_dict = ALT_library.build_trie(vocabulary)


    # check_distances_trie("./result_damerau-levenshtein_quijote.txt", trie_dict, distance_damerau_trie)
    # check_distances_trie("./result_levenshtein_quijote.txt", trie_dict, distance_trie)
    # check_distances_trie_bb("./result_levenshtein_quijote.txt", trie_dict, branch_and_bound_trie)

    # check_distances_trie_bb("./result_damerau-levenshtein_quijote.txt", trie_dict, distance_damerau_trie_pruned)
    # check_distances_trie_bb("./result_damerau-levenshtein_quijote.txt", trie_dict, branch_and_bound_trie_damerau)
