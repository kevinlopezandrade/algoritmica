# We will do benchmarks for the
# 1.- Threshold
# 2.- Size of the vocabulary

import time
import numpy as np

import trie
import ALT_library

from distances import *
from ALT_library import search_below_threshold



PATH = "corpus/quijote.txt"
vocabulary = list(ALT_library.get_vocabulary(PATH))
string = "casa"
N = len(vocabulary)


# print("x 1 2 3 4 5")

# for n in np.linspace(100, N, num=20):
#     n = int(n)

#     trie_dict = ALT_library.build_trie(vocabulary[:n])

#     print(n, end=" ")

#     for k in [1,2,3,4,5]:
#         t1 = time.time()
#         # search_below_threshold(string, vocabulary[:n], k, levenshtein_distance)
#         # search_below_threshold(string, vocabulary[:n], k, damerau_levenshtein_distance) 
#         # trie_distance(string, trie_dict, k)
#         # damerau_trie_distance(string, trie_dict, k)
#         # branch_and_bound_distance(string, trie_dict, k)
#         # damerau_branch_and_bound_distance(string, trie_dict, k)
#         t2 = time.time()
#         print("%2.4f" % (t2 - t1), end=" ")
    
#     print("")


print("x levenshtein damerau-levenshtein levenshtein-trie damerau-levenshtein-trie levenshtein-rp damerau-levenshtein-rp")

k = 5

functions = [search_below_threshold, search_below_threshold, trie_distance, damerau_trie_distance, branch_and_bound_distance, damerau_branch_and_bound_distance]

for n in np.linspace(100, N, num=20):
    n = int(n)

    trie_dict = ALT_library.build_trie(vocabulary[:n])

    print(n, end=" ")

    for i, funct in enumerate(functions):
        if i == 0:
            t1 = time.time()
            funct(string, vocabulary[:n], k, levenshtein_distance)
            t2 = time.time()
            print("%2.4f" % (t2 - t1), end=" ")
        elif i == 1:
            t1 = time.time()
            funct(string, vocabulary[:n], k, damerau_levenshtein_distance)
            t2 = time.time()
            print("%2.4f" % (t2 - t1), end=" ")
        else:
            t1 = time.time()
            funct(string, trie_dict, k)
            t2 = time.time()
            print("%2.4f" % (t2 - t1), end=" ")

    print("")
