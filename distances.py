import numpy as np
from collections import deque

from math import inf

def levenshtein_distance(x, y):
    """ Computes the levenshtein distance

    This implementation comes from the provided book
    """

    D = {}
    D[0, 0] = 0
    for i in range(1, len(x)+1):
        D[i, 0] = D[i-1, 0] + 1
    
    for j in range(1, len(y)+1):
        D[0, j] = D[0, j-1] + 1
        for i in range(1, len(x)+1):
            D[i, j] = min(D[i-1, j] + 1, D[i, j-1] +1, D[i-1, j-1] + (x[i-1] != y[j-1]))
    
    return D[len(x), len(y)]


def damerau_levenshtein_distance(x, y):
    """Computes the damerau levenshtein distance

    This function computes the edition distance between two strings by transversing the matrix by columns

    Args:
        x:str 
        y:str

    Returns:
        Integer value of the edition distance


    """

    D = {}

    D[0,0] = 0

    N_x = len(x)
    N_y = len(y)

    # Column is the first index in the tuple (x,y)
    # WARNING: If something fails in the future this code below
    # is probably the responsable for that
    
    # Fill column zero 
    for row in range(1, N_y + 1):
        D[0, row] = row

    # Fill row zero
    for column in range(1, N_x + 1):
        D[column, 0] = column

    # Fill the full matrix
    for column in range(1, N_x + 1):
        for row in range(1, N_y + 1):
            if column == 1 or row == 1:
                D[column, row] = min(D[column - 1, row-1] + (1 if x[column-1] != y[row-1] else 0),
                                     D[column, row - 1] + 1,
                                     D[column - 1, row] + 1)
            else:
                D[column, row] = min(D[column - 1, row-1] + (1 if x[column-1] != y[row-1] else 0),
                                     D[column, row - 1] + 1,
                                     D[column - 1, row] + 1,
                                     D[column - 2, row - 2] + 1 if x[column-1] == y[row-1-1] and x[column - 1 - 1] == y[row - 1] else inf)
    
    return D[N_x, N_y]


def trie_distance(string, trie, threshold):

    distances = {}

    def levenshtein_trie_distance(string, node, parent_row=None, depth=0, prefix_word=""):
        """Performs a depth first search while its constructing the corresponding row"""

        is_root = True if node.parent is None else False

        if is_root:
            row = np.zeros(len(string) + 1, dtype=np.int32) 

            row[0] = 0

            for i in range(1, len(string) + 1):
                row[i] = i


        else:
            row =  np.zeros(len(string) + 1, dtype=np.int32)

            row[0] = depth

            for i in range(1, len(string) + 1):
                row[i] = min(
                    row[i-1] + 1,
                    parent_row[i] + 1,
                    parent_row[i-1] + (string[i-1] != node.char)
                )

            prefix_word = prefix_word + node.char

        if node.is_final and row[len(string)] <= threshold:
            distances[prefix_word] = row[len(string)]


        if np.min(row) <= threshold:
            for char in node.children:
                levenshtein_trie_distance(string, node.children[char], parent_row=row, depth=depth+1, prefix_word=prefix_word)


    levenshtein_trie_distance(string, trie.root_node)

    return distances


def damerau_trie_distance(string, trie, threshold):
    distances = {}

    def damerau_trie(string, node, prev_matrix=None, depth=0, prefix_word=""):
        """For the damerau distance we need to save the two parents before the
        current one"""

        is_root = True if node.parent is None else False

        if is_root:
            row = np.zeros(len(string) + 1, dtype=np.int32)
            row[0] = 0

            for column in range(1, len(string) + 1):
                row[column] = column
        
            matrix = np.zeros((2, len(string) + 1), dtype=np.int32)
            matrix[0,:] = row


        else:
            # Convention
            # Row = 0 in the prev_matrix is the parent_row 
            # Row = 1 in the prev_matrix is the parent_parent_row

            prefix_word = prefix_word + node.char


            row = np.zeros(len(string) + 1, dtype=np.int32)

            row[0] = depth

            for column in range(1, len(string) + 1):
                if depth == 1 or column == 1:
                    row[column] = min(
                        row[column - 1] + 1,
                        prev_matrix[0, column - 1] + (string[column - 1] != node.char),
                        prev_matrix[0, column] + 1
                    )
                elif depth > 1 and column > 1:
                    row[column] = min(
                        row[column - 1] + 1,
                        prev_matrix[0, column - 1] + (string[column - 1] != node.char),
                        prev_matrix[0, column] + 1,
                        prev_matrix[1, column - 2] + 1 if (string[column - 1] == prefix_word[-2] and string[column - 2] == node.char) else inf
                    )


            # Interchange the rows
            matrix = np.zeros((2, len(string) + 1), dtype=np.int32)
            matrix[1,:] = prev_matrix[0, :]
            matrix[0,:] = row


        if node.is_final and row[len(string)] <= threshold:
            distances[prefix_word] = row[len(string)]

        if np.min(row) <= threshold:
            for char in node.children:
                damerau_trie(string, node.children[char], prev_matrix=matrix, depth=depth+1, prefix_word=prefix_word)

    damerau_trie(string, trie.root_node)

    return distances

def branch_and_bound_distance(string, trie, threshold):

    distances = {}

    def branch(state, A):
        index    = state[0]
        node     = state[1]
        distance = state[2]

        # Save here if its final state
        if index == len(string) and node.is_final:
            # Ask here
            distances[node.word] = min(distance, distances.get(node.word, inf))

        if index <= len(string):
            # Insertion
            for char in node.children:
                A.append((index, node.children[char], distance + 1))

        if index <= len(string) - 1:

            # Borrado
            A.append((index + 1, node, distance + 1))

            # Subsitution 
            for char in node.children:
                A.append((index + 1, node.children[char], distance + (string[index] != char)))


    initial_state = (0, trie.root_node, 0)

    A = deque()

    A.append(initial_state)

    while len(A) != 0:

        state = A.pop()

        distance = state[2]

        # Prune here by factibility
        if distance <= threshold:
            branch(state, A)

    return distances

def damerau_branch_and_bound_distance(string, trie, threshold):

    distances = {}

    def branch(state, A):
        index    = state[0]
        node     = state[1]
        distance = state[2]

        # Save here if its final state
        if index == len(string) and node.is_final:
            # Ask here
            distances[node.word] = min(distance, distances.get(node.word, inf))

        if index <= len(string):
            # Insertion
            for char in node.children:
                A.append((index, node.children[char], distance + 1))

        if index <= len(string) - 1:

            # Borrado
            A.append((index + 1, node, distance + 1))

            # Subsitution 
            for char in node.children:
                A.append((index + 1, node.children[char], distance + (string[index] != char)))

            if index <= len(string) - 2:
                # Tranposition
                for char in node.children:
                    node_child = node.children[char]

                    for char_child in node_child.children:
                        if string[index] == char_child and  string[index + 1] == char:
                            A.append((index + 2, node_child.children[char_child], distance + 1))


    initial_state = (0, trie.root_node, 0)

    A = deque()

    A.append(initial_state)

    while len(A) != 0:
        # breakpoint()

        state = A.pop()

        distance = state[2]

        # Prune here by factibility
        if distance <= threshold:
            branch(state, A)

    return distances
